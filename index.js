const express = require('express');
const mercadopago = require('mercadopago')
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.listen(9000, () => console.log('Servidor iniciado na porta 9000'));

mercadopago.configure({
    access_token: 'APP_USR-2560380992853730-082322-3207791589e926eb48daa18d67ac7c3e-670928850'
});

app.post('/mercadopago/checkout', (req, res) => {
    mercadopago.preferences.create(req.body.observe).then(
        function(response){
            data = {
                "url": response.body.init_point
            }
            res.send(data);
        }).catch(function(error){
            console.log(error);
            res.send(error);
        });
});

app.post('/mercadopago/status', (req, res) => {

    var filters = {
        id: req.body.observe.id,
    };
    mercadopago.payment.search({
        qs: filters
    }).then(function (data) {
        res.send(data.body.results[0].status);
    }).catch(function (error) {
        res.render('500', {
            error: error
        });
    });
});